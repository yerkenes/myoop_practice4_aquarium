public class Fish extends Animal {
    private double speedInWater;
    private double tailSize;
    private double sumKZT;

    public Fish(String name, double weight, String color, double speedInWater, double tailSize, double sum) {
        super(name, weight, color); // constructor with inheritance
        this.speedInWater = speedInWater;
        this.tailSize = tailSize;
        this.sumKZT = sum;
    }

    public Fish() { // empty constructor
    }

    //getter and setter

    public double getSpeedInWater() {
        return speedInWater;
    }

    public void setSpeedInWater(double speedInWater) {
        this.speedInWater = speedInWater;
    }

    public double getTailSize() {
        return tailSize;
    }

    public void setTailSize(double tailSize) {
        this.tailSize = tailSize;
    }

    public double getSum() {
        return sumKZT;
    }

    public void setSum(double sum) {
        this.sumKZT = sum;
    }

    @Override
    public String toString() {
        return "Fish{" +
                "speedInWater=" + speedInWater +
                ", tailSize=" + tailSize +
                ", sum=" + sumKZT +
                '}';
    }
}
