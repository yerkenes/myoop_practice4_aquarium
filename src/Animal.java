public abstract class Animal {
    private String name;
    private double weight;
    private  String color;

    public Animal(String name, double weight, String color) { // Create constructor
        this.name = name;
        this.weight = weight;
        this.color = color;
    }

    public  Animal() {

    } // Empty

    // Getter and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                ", color='" + color + '\'' +
                '}';
    } // toString to print data
}
