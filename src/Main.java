import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Fish> arrayListFish = new ArrayList<>();

        Fish carp = new Fish("Carp", 3.6, "white", 16, 3.4, 15000);
        Fish pike = new Fish("Pike", 6.2, "black", 26, 7.4, 60000);
        Fish swordFish = new Fish("Pike", 12, "black", 10, 2.2, 3000);
        Fish goldFish = new Fish("Gold Fish", 25, "gold", 15, 2, 95000);

        arrayListFish.add(carp);
        arrayListFish.add(pike);
        arrayListFish.add(swordFish);
        arrayListFish.add(goldFish);

        // Show all fishes
        Aquarium aquarium = new Aquarium(arrayListFish);
        System.out.println(aquarium.print());

        // Show total sum
        aquarium.sum();





    }
}
