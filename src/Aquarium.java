import java.util.ArrayList;

public class Aquarium {
    public double totalSum = 0;
    private ArrayList<Fish> arrayListFish;

    public Aquarium( ArrayList<Fish> arrayListFish) {
        this.arrayListFish = arrayListFish;
    }

    public ArrayList<Fish> print() {
        return arrayListFish;
    }

    public void sum() {
        for(int i = 0; i < arrayListFish.size(); i++) {
            totalSum += arrayListFish.get(i).getSum();
        }

        System.out.printf(String.valueOf(totalSum));
    }
}
